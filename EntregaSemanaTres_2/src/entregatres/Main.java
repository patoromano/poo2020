package entregatres;

import java.awt.*;
import java.awt.event.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.*;

public class Main extends JFrame implements ActionListener {

	private JMenuBar menuBar;
	private JMenu menuOpciones, menuAyuda, menuColorFondo;
	private JMenuItem mItemGris, mItemNegro, mItemCreador, mItemSalir, mItemNuevo;
	private JLabel labelTitle, labelFooter;
	private JTextField txtUrl;
	private JButton boton1;
	public static String texto = ""; //ojo aca, ver como lo usas en Ventana Imagen

	public Main() {
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Pantalla principal");
		getContentPane().setBackground(new Color(200, 200, 200));
		setIconImage(new ImageIcon(getClass().getResource("images/heart.png")).getImage());

		menuBar = new JMenuBar();
		menuBar.setBackground(new Color(255, 255, 255));
		setJMenuBar(menuBar);

		menuOpciones = new JMenu("Opciones");
		menuOpciones.setBackground(new Color(255, 0, 0));
		menuOpciones.setFont(new Font("Verdana", 1, 12));
		menuOpciones.setForeground(new Color(255, 128, 0));
		menuBar.add(menuOpciones);

		menuAyuda = new JMenu("Acerca de");
		menuAyuda.setBackground(new Color(255, 0, 0));
		menuAyuda.setFont(new Font("Verdana", 1, 12));
		menuAyuda.setForeground(new Color(255, 128, 0));
		menuBar.add(menuAyuda);

		menuColorFondo = new JMenu("Color de fondo");
		menuColorFondo.setFont(new Font("Verdana", 1, 12));
		menuColorFondo.setForeground(new Color(255, 128, 0));
		menuOpciones.add(menuColorFondo);

		mItemNegro = new JMenuItem("Oscuro");
		mItemNegro.setFont(new Font("Verdana", 1, 12));
		mItemNegro.setForeground(new Color(255, 128, 0));
		menuColorFondo.add(mItemNegro);
		mItemNegro.addActionListener(this);

		mItemGris = new JMenuItem("Claro");
		mItemGris.setFont(new Font("Verdana", 1, 12));
		mItemGris.setForeground(new Color(255, 128, 0));
		menuColorFondo.add(mItemGris);
		mItemGris.addActionListener(this);

		mItemNuevo = new JMenuItem("Nuevo");
		mItemNuevo.setFont(new Font("Verdana", 1, 12));
		mItemNuevo.setForeground(new Color(255, 128, 0));
		menuOpciones.add(mItemNuevo);
		mItemNuevo.addActionListener(this);

		mItemCreador = new JMenuItem("El creador");
		mItemCreador.setFont(new Font("Verdana", 3, 12));
		mItemCreador.setForeground(new Color(255, 128, 0));
		menuAyuda.add(mItemCreador);
		mItemCreador.addActionListener(this);

		mItemSalir = new JMenuItem("Salir");
		mItemSalir.setFont(new Font("Verdana", 1, 12));
		mItemSalir.setForeground(new Color(255, 128, 0));
		menuOpciones.add(mItemSalir);
		mItemSalir.addActionListener(this);

		labelTitle = new JLabel("URL para visualizar la imagen");
		labelTitle.setBounds(25, 30, 900, 25);
		labelTitle.setFont(new Font("Verdana", 1, 16));
		labelTitle.setForeground(new Color(0, 0, 0));
		add(labelTitle);

		txtUrl = new JTextField();
		txtUrl.setBounds(25, 55, 500, 25);
		txtUrl.setBackground(new java.awt.Color(224, 224, 224));
		txtUrl.setFont(new java.awt.Font("Verdana", 1, 14));
		txtUrl.setForeground(new java.awt.Color(0, 0, 0));
		add(txtUrl);

		labelFooter = new JLabel("Visualizador de Im�genes");
		labelFooter.setBounds(135, 445, 500, 30);
		labelFooter.setFont(new java.awt.Font("Verdana", 1, 12));
		labelFooter.setForeground(new java.awt.Color(255, 255, 255));
		add(labelFooter);

		boton1 = new JButton("Visualizar");
		boton1.setBounds(540, 55, 100, 25);
		boton1.setBackground(new Color(255, 128, 0));
		boton1.setFont(new Font("Verdana", 1, 12));
		boton1.setForeground(new Color(0, 0, 0));
		boton1.addActionListener(this);
		add(boton1);

	}

	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == mItemGris) {
			getContentPane().setBackground(new Color(200, 200, 200));
			menuBar.setBackground(new Color(255, 255, 255));
			labelTitle.setForeground(new Color(0, 0, 0));
		}
		if (e.getSource() == mItemNegro) {
			getContentPane().setBackground(new Color(0, 0, 0));
			menuBar.setBackground(new Color(0, 0, 0));
			labelTitle.setForeground(new Color(255, 128, 0));

		}

		if (e.getSource() == mItemNuevo) {
			txtUrl.setText("");
		}

		if (e.getSource() == mItemSalir) {
			this.setVisible(false);
		}

		if (e.getSource() == mItemCreador) {

			JOptionPane.showMessageDialog(null, "Desarrollado por Pablo Romano");
		}

		if (e.getSource() == boton1) {
			texto = txtUrl.getText().trim();
			Path path = Paths.get(texto);
			if (texto.equals("")) {
				JOptionPane.showMessageDialog(null, "Debes ingresar una URL.");
			} else if (Files.exists(path)) {
				VentanaImagen ventanaimagen = new VentanaImagen();
				ventanaimagen.setBounds(0, 0, 600, 600);
				ventanaimagen.setVisible(true);
				ventanaimagen.setLayout(new FlowLayout());
				ventanaimagen.setResizable(true);
				ventanaimagen.setLocationRelativeTo(null);
				this.setVisible(false);
			} else {
				JOptionPane.showMessageDialog(null, "Debes ingresar una URL v�lida.");
			}

		}

	}

	public static void main(String args[]) {
		Main ventanaPrincipal = new Main();
		ventanaPrincipal.setBounds(0, 0, 750, 180);
		ventanaPrincipal.setVisible(true);
		ventanaPrincipal.setResizable(false);
		ventanaPrincipal.setLocationRelativeTo(null);

	}

}
