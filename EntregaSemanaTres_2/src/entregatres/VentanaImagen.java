package entregatres;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class VentanaImagen extends JFrame implements ActionListener {
	
	private JLabel labelImagen;
	private JMenuBar mb;
	private JMenu menuOpciones;
	private JMenuItem miSalir, miVolver;
	String urlImagen = "";
	

	
	public VentanaImagen() {
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Pantalla de visualización");
		getContentPane().setBackground(new Color(220, 220, 220));
		setIconImage(new ImageIcon(getClass().getResource("images/heart.png")).getImage());
		Main ventanaPrincipal = new Main();
		urlImagen = ventanaPrincipal.texto;
		/*el atributo texto en Main es static, es como si fuera una variable global. 
		Para usarlas no hace falta instanciar un objeto, podrias reemplazar "ventanaPrincipal.texto"
		por "Main.texto". Ahora hubiera sido mejor si el valor del texto lo pasabas como parametro 
		del constructor de esta ventana, al tener un comportamiento de variable global cualquier uso 
		en otra parte del codigo puede alterar su valor y obtener resultados inesperados, en programas
		mas grandes puede llegar a ser un problema    
		*/

		
		mb = new JMenuBar();
		mb.setBackground(new Color(0,0,0));
		setJMenuBar(mb);
		
		menuOpciones = new JMenu("Opciones");
		menuOpciones.setBackground(new Color(0,0,0));
		menuOpciones.setFont(new Font("Verdana", 1, 12));
		menuOpciones.setForeground(new Color(255,128,0));
		mb.add(menuOpciones);
		
		miSalir = new JMenuItem("Salir");
		miSalir.setFont(new Font("Verdana", 1, 12));
		miSalir.setForeground(new Color(255,128,0));
		menuOpciones.add(miSalir);
		miSalir.addActionListener(this);
		
		miVolver = new JMenuItem("Volver");
		miVolver.setFont(new Font("Verdana", 1, 12));
		miVolver.setForeground(new Color(255,128,0));
		menuOpciones.add(miVolver);
		miVolver.addActionListener(this);
		
		labelImagen = new JLabel();
		labelImagen.setIcon(new ImageIcon(urlImagen));
		labelImagen.setBounds(95,60,500,500);
		add(labelImagen);
		
	}

	
	
	
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == miVolver) {
			Main ventanaPrincipal = new Main();
			ventanaPrincipal.setBounds(0,0,750,180);
			ventanaPrincipal.setVisible(true);
			ventanaPrincipal.setResizable(true);
			ventanaPrincipal.setLocationRelativeTo(null);
			this.setVisible(false);
		}
		
		if (e.getSource() == miSalir) {
			this.setVisible(false);
		}
	}
	

}
