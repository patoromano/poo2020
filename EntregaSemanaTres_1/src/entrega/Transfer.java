package entrega;

import java.util.HashMap;
import java.util.Random;

public class Transfer {
	

	public Cuenta comprarDolares(Double dolares, Cuenta cuenta) {
		Double pesosGastados = dolares * 76.25;
			Double totalDolares = dolares + cuenta.getDolares();
			cuenta.setDolares(totalDolares);
			Double totalPesos = cuenta.getPesos() - pesosGastados;
			cuenta.setPesos(totalPesos);
			cuenta.setSigno(" Compr�");
			System.out.println("--------------------------------------");
			System.out.println("Has realizado la compra con �xito.");
			System.out.println("--------------------------------------");
		return cuenta;
	}

	
	
	public Cuenta venderDolares(Double dolares, Cuenta cuenta) {
			Double pesosGanados = dolares*76.25;
			Double totalDolares = cuenta.getDolares()- dolares;
			cuenta.setDolares(totalDolares);
			Double totalPesos = cuenta.getPesos() + pesosGanados;
			cuenta.setPesos(totalPesos);
			cuenta.setSigno(" Vendi�");
			System.out.println("--------------------------------------");
			System.out.println("Has realizado la venta con �xito.");
			System.out.println("--------------------------------------");
		return cuenta;
	}

	
	
	public HashMap<String, String> listarTransferencias(String signo, Double transfer,HashMap<String, String> transferencias) {
		Random rnd = new Random();
		Integer mes = rnd.nextInt(12)+1;
		Integer dia = rnd.nextInt(31)+1;
		String fecha = "";
		String tra = signo+" "+transfer+" d�lares.";
		fecha = " (2020 - "+mes+" - "+dia+") ";
		transferencias.put(fecha, tra);
		return transferencias;
	}

}