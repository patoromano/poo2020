package entrega;

import java.security.SecureRandom;

public class Cuenta {

	private Double pesos;
	private Double dolares;
	private String signo;
	
	public Cuenta() {
		this(0.0, 0.0, "");
		this.setPesos(autoGenPesos());
	
	}

	public Cuenta(Double pesos, Double dolares, String signo) {
		super();
		this.setPesos(pesos);
		this.setDolares(dolares);
		this.setSigno(signo);
	}

	public Double getPesos() {
		return pesos;
	}

	public void setPesos(Double pesos) {
		this.pesos = pesos;
	}

	public Double getDolares() {
		return dolares;
	}

	public void setDolares(Double dolares) {
		this.dolares = dolares;
	}
	

	public String getSigno() {
		return signo;
	}

	public void setSigno(String signo) {
		this.signo = signo;
	}

	
	private Double autoGen(Double min, Double max) {
		SecureRandom rnd = new SecureRandom();
		Double num = min+(max-min)*rnd.nextDouble();
		return num;
	}
	
	public Double autoGenPesos() {
		Double min = 1000.00;
		Double max = 2000.00;
		return autoGen(min, max);
	}
}
