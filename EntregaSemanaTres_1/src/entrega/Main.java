package entrega;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Scanner;

import com.google.gson.Gson;

public class Main {

	public static void main(String[] args) {

		URL url;
		try {
			// ASIGNO LA URL
			url = new URL("https://raw.githubusercontent.com/alevega/datosCambio/master/datos.json");
			// ABRO UNA CONEXION
			URLConnection conexion = url.openConnection();
			// LEER CONTENIDO PAGINA
			BufferedReader lectura = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

			String datos = "";
			String linea;
			while ((linea = lectura.readLine()) != null) {
				datos = datos + linea;
			}
			HashMap<String, Double> hashmap = new Gson().fromJson(datos, HashMap.class); // SI TRABAJO LAS FECHAS COMO
																							// STRING PUEDO HACERLO ASI

			System.out.println(hashmap);

		} catch (IOException e) {
			e.printStackTrace();
		}

		Scanner userInput = new Scanner(System.in);
		Cuenta cuenta = new Cuenta();
		Transfer transfer = new Transfer();
		HashMap<String, String> transferencias = new HashMap<String, String>();
		Double dolares = 0.0;
		String signo = "";
		Integer bandera = 0;
		Integer seleccion = 0;
		DecimalFormat df = new DecimalFormat("#.00");

		do {
			
			
			
			do {
				System.out.println("\n �Que operaci�n desea realizar? ");
				System.out.println(" 		1. Comprar Dolares. ");
				System.out.println("		2. Vender Dolares. ");
				System.out.println("		3. Mostrar Transferencias. ");
				System.out.println("		4. Mostrar Saldo. ");
				System.out.println("		5. Salir.");
				seleccion = userInput.nextInt();

				if (seleccion >= 1 && seleccion <= 5) {
					bandera = 1;
				} else {
					System.out.println("--------------------------------------------------");
					System.out.println("Opci�n no disponible, vuelve a intentar por favor.");
					System.out.println("--------------------------------------------------");
				}
			} while (bandera == 0);
			
			
			

			if (seleccion == 1) {
				System.out.println("Tu saldo en pesos es de: " + df.format(cuenta.getPesos()));
				System.out.println("�Cu�ntos d�lares desea comprar?");
				dolares = userInput.nextDouble();

				if (dolares == 0) {
					System.out.println("No puedes comprar esa cantidad.");
				} else if (dolares * 76.25 > cuenta.getPesos()) {
					System.out.println("--------------------------------------");
					System.out.println("Tu saldo en pesos no es suficiente.");
					System.out.println("--------------------------------------");
				} else {
					cuenta = transfer.comprarDolares(dolares, cuenta);
					Double dolar = cuenta.getDolares();
					signo = cuenta.getSigno();
					transfer.listarTransferencias(signo, dolares, transferencias);
				}
				
				
				
				
			} else if (seleccion == 2) {
				System.out.println("Tu saldo en d�lares es de: " + df.format(cuenta.getDolares()));
				System.out.println("�Cu�ntos d�lares desea vender?");
				dolares = userInput.nextDouble();
				if (dolares <= 0) {
					System.out.println("No puedes vender esa cantidad.");
				} else if (dolares > cuenta.getDolares()){
					System.out.println("--------------------------------------");
					System.out.println("Tu saldo en d�lares no es suficiente.");
					System.out.println("--------------------------------------");
				} else {
					cuenta = transfer.venderDolares(dolares, cuenta);
					Double dolar = cuenta.getDolares();
					signo = cuenta.getSigno();
					transfer.listarTransferencias(signo, dolares, transferencias);
				}
				
				
				
			} else if (seleccion == 3) {
				System.out.println("----------------------");
				System.out.println("    Transferencias:   ");
				System.out.println("----------------------\n\n");
				System.out.println(transferencias);

				
				
				
			} else if (seleccion == 4) {
				System.out.println("--------------------------------------");
				System.out.println("Saldo en pesos: " + df.format(cuenta.getPesos()));
				System.out.println("Saldo en d�lares: " + df.format(cuenta.getDolares()));
				System.out.println("--------------------------------------");

				
				
				
			} else if (seleccion == 5) {
				System.out.println("-------------------------");
				System.out.println("�Gracias!, vuelva pronto.");
				System.out.println("-------------------------");
				bandera = 2;
			}
			
			
			
		} while (bandera != 2);

	}

}

/*
Tenes bastantes cosas incorrectas.
1- tenes mal hecho lo de la compra de dolar y pesos, usas una cotizacion fija
2- lo mismo pasa con las fechas decis que pasan todas en este a�o y no era asi
3- en base a esto no podrias mostrar la fluctacion del precio del dolar

Te comento la idea era que uses los datos obtenidos del Json por eso tienen una fecha y una cotizacion.
Basicamente es obtener el primer dia del json y comprar dolares con esa cotizacion para que luego en la siguiente compra
avance una X cantidad de dias y busque otra fecha con otra cotizacion.
eso lo podes ir mostrando en el momento o guardarlo, esto es como vos quieras
para luego poder mostrar todas las compras que hizo y ver a cuanto compro y en fecha

seria algo asi:
 Compra Dolar: 200 Cotizacion: 23.70 pesos: 100 Fecha: 2017/09/10

como un ejemplo podrias hacerlo algo asi
*/