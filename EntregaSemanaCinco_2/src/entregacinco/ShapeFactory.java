package entregacinco;

public class ShapeFactory {
	
	Circle circle = new Circle();
	Square square = new Square();
	Rectangle rectangle = new Rectangle();
	
	public Shape getShape (String shape) {
		
		if (shape.equals("Circle")) {
			return circle;
		} else if (shape.equals("Square")) {
			return square;
		} else {
			return rectangle;
		}
	}

}
