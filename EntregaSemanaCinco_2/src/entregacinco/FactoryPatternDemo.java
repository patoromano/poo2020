package entregacinco;

import javax.swing.JOptionPane;

public class FactoryPatternDemo {
	
	static String figure, option;

	public static void main(String[] args) {
		option = (JOptionPane.showInputDialog(null,
				"Selecciona la figura deseada: \n" + "1. Circulo \n" + "2. Cuadrado \n"
						+ "3. Rectangulo \n","Opciones", JOptionPane.PLAIN_MESSAGE, null, 
						new Object[] { "Selecciona", "1", "2", "3" },
				"Selecciona")).toString();

		switch(option) {
		case "1": figure = "Circle";
		break;
		case "2": figure = "Square";
		break;
		case "3": figure = "Rectangle";
		break;
		}
		
		ShapeFactory shapefactory = new ShapeFactory();
		
		Shape shape = shapefactory.getShape(figure);
		shape.draw();
	}    
}
