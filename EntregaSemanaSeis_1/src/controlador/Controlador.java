package controlador;

import java.awt.event.*;

import javax.swing.RowFilter;
import javax.swing.event.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import modelo.Usuario;
import vista.NuevoUsuario;
import vista.Vista;

public class Controlador implements ListSelectionListener, ActionListener, KeyListener {

	private Vista vista;
	private NuevoUsuario nuevousuario;

	public Controlador() {
		super();
		this.setVista(new Vista(this));
		this.setNuevoUsuario(new NuevoUsuario(this));
		Usuario usuario;
		for (int i = 0; i < 4; i++) {
			usuario = new Usuario((i + 1), "Nombre" + (i + 1), "Apellido" + (i + 1), "444444" + (i + 1));
			Object[] row = { String.valueOf(usuario.getId()), usuario.getNombre(), usuario.getApellido(),
					usuario.getTelefono() };
			this.getVista().getModeloTabla().addRow(row);
		}

		this.getVista().setVisible(true);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		this.getVista().getBtDelete().setEnabled(true);
		this.getVista().getBtEdit().setEnabled(true);
		this.getVista().getBtInfo().setEnabled(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(this.getVista().getBtInfo())) {
			this.getNuevoUsuario().setVisible(true);
			this.getNuevoUsuario().getlTitulo().setText("Información Usuario");
			String texto = (String) this.getVista().getTable().getValueAt(this.getVista().getTable().getSelectedRow(),
					1);
			this.getNuevoUsuario().getTxtNombre().setText(texto);
			this.getNuevoUsuario().getTxtNombre().setEditable(false);
			texto = (String) this.getVista().getTable().getValueAt(this.getVista().getTable().getSelectedRow(), 2);
			this.getNuevoUsuario().getTxtApellido().setText(texto);
			this.getNuevoUsuario().getTxtApellido().setEditable(false);
			texto = (String) this.getVista().getTable().getValueAt(this.getVista().getTable().getSelectedRow(), 3);
			this.getNuevoUsuario().getTxtTelefono().setText(texto);
			this.getNuevoUsuario().getTxtTelefono().setEditable(false);
		}

		if (e.getSource().equals(this.getVista().getBtEdit())) {
			this.getNuevoUsuario().setVisible(true);
			this.getNuevoUsuario().getlTitulo().setText("Modificar Usuario");
			this.getVista().getModeloTabla().setValueAt(this.getNuevoUsuario().getTxtNombre().getText(),
					this.getVista().getTable().getSelectedRow(), 1);
			this.getVista().getModeloTabla().setValueAt(this.getNuevoUsuario().getTxtApellido().getText(),
					this.getVista().getTable().getSelectedRow(), 2);
			this.getVista().getModeloTabla().setValueAt(this.getNuevoUsuario().getTxtTelefono().getText(),
					this.getVista().getTable().getSelectedRow(), 3);
		}

		if (e.getSource().equals(this.getVista().getBtDelete())) {
			this.getVista().getModeloTabla().removeRow(this.getVista().getTable().getSelectedRow());
		}

		if (e.getSource().equals(this.getVista().getBtNew())) {
			this.getNuevoUsuario().setVisible(true);
		}

		if (e.getSource().equals(this.getNuevoUsuario().getbAceptar())) {
			this.getNuevoUsuario().setVisible(false);
			String nom = this.getNuevoUsuario().getTxtNombre().getText();
			String ape = this.getNuevoUsuario().getTxtApellido().getText();
			String tel = this.getNuevoUsuario().getTxtTelefono().getText();
			Integer value = this.getVista().getModeloTabla().getRowCount() + 1;
			String id = (String) this.getVista().getModeloTabla().getValueAt(value, 0);
			Object[] row = { id, nom, ape, tel };
			this.getVista().getModeloTabla().addRow(row);
		}

		if (e.getSource().equals(this.getNuevoUsuario().getbCancelar())) {
			this.getNuevoUsuario().setVisible(false);
		}

	}

	public void search() {
		TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(this.getVista().getModeloTabla());
		this.getVista().getTable().setRowSorter(tr);
		tr.setRowFilter(RowFilter.regexFilter(this.getVista().getTxtBuscar().getText()));
	}

	@Override
	public void keyPressed(KeyEvent e) {
		this.search();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		this.search();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	public Vista getVista() {
		return vista;
	}

	public void setVista(Vista vista) {
		this.vista = vista;
	}

	public NuevoUsuario getNuevoUsuario() {
		return nuevousuario;
	}

	public void setNuevoUsuario(NuevoUsuario nuevousuario) {
		this.nuevousuario = nuevousuario;
	}

}
