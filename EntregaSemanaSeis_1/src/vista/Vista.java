package vista;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import controlador.Controlador;

public class Vista extends JFrame {
	
	private JPanel contentPane;
	private JTable table;
	private JButton btNew, btEdit, btDelete, btInfo;
	private DefaultTableModel modeloTabla;
	private JTextField txtBuscar;
	private Controlador controlador;

	
	


	public Vista(Controlador controlador) {
		this.setControlador(controlador);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(100,100,485, 300);
		setTitle("ABM");
		contentPane= new JPanel();
		contentPane.setBorder(new EmptyBorder(5,5,5,5));
		setLocationRelativeTo(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsuarios = new JLabel("Usuarios");
		lblUsuarios.setBounds(165,5,100,30);
		lblUsuarios.setFont(new Font("Verdana", 1, 20));
		contentPane.add(lblUsuarios);
		
		JLabel lblBuscar = new JLabel("Buscar: ");
		lblBuscar.setBounds(50,20,100,50);
		contentPane.add(lblBuscar);
		
		txtBuscar = new JTextField();
		txtBuscar.setBounds(100,35,220,20);
		contentPane.add(txtBuscar);
		txtBuscar.setColumns(10);
		txtBuscar.addKeyListener(this.getControlador());
		
		JPanel panel = new JPanel();
		panel.setBounds(10,57,327,193);
		contentPane.add(panel);
		panel.setLayout(null);
		
		String header[] = {"ID", "Nombre", "Apellido", "Telefono"};
		
		modeloTabla = new DefaultTableModel(null, header);
		
		table = new JTable(modeloTabla);
		table.setBounds(0,0,327,193);
		table.setVisible(true);
		table.setDefaultEditor(Object.class, null);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(0).setPreferredWidth(0);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getSelectionModel().addListSelectionListener(this.getControlador());

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0,0,327,193);
		scrollPane.setViewportView(table);
		panel.add(scrollPane);
		

		
		btNew = new JButton("Nuevo");
		btNew.setEnabled(true);
		btNew.setBounds(347,58,110,23);
		contentPane.add(btNew);
		btNew.addActionListener(this.getControlador());
		
		btEdit = new JButton("Modificar");
		btEdit.setEnabled(false);
		btEdit.setBounds(347,98,110,23);
		contentPane.add(btEdit);
		btEdit.addActionListener(this.getControlador());
		
		btDelete = new JButton("Eliminar");
		btDelete.setEnabled(false);
		btDelete.setBounds(347,138,110,23);
		contentPane.add(btDelete);
		btDelete.addActionListener(this.getControlador());
		
		btInfo = new JButton("Información");
		btInfo.setEnabled(false);
		btInfo.setBounds(347,178,110,23);
		contentPane.add(btInfo);
		btInfo.addActionListener(this.getControlador());
		
		
		
	}


	public JTextField getTxtBuscar() {
		return txtBuscar;
	}


	public void setTxtBuscar(JTextField txtBuscar) {
		this.txtBuscar = txtBuscar;
	}


	public JTable getTable() {
		return table;
	}


	public void setTable(JTable table) {
		this.table = table;
	}


	public JButton getBtNew() {
		return btNew;
	}


	public void setBtNew(JButton btNew) {
		this.btNew = btNew;
	}


	public JButton getBtEdit() {
		return btEdit;
	}


	public void setBtEdit(JButton btEdit) {
		this.btEdit = btEdit;
	}


	public JButton getBtDelete() {
		return btDelete;
	}


	public void setBtDelete(JButton btDelete) {
		this.btDelete = btDelete;
	}


	public JButton getBtInfo() {
		return btInfo;
	}


	public void setBtInfo(JButton btInfo) {
		this.btInfo = btInfo;
	}


	public DefaultTableModel getModeloTabla() {
		return modeloTabla;
	}


	public void setModeloTabla(DefaultTableModel modeloTabla) {
		this.modeloTabla = modeloTabla;
	}
	
	public Controlador getControlador() {
		return controlador;
	}


	public void setControlador(Controlador controlador) {
		this.controlador = controlador;
	}

	
}
