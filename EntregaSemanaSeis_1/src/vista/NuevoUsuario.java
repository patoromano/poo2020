package vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.*;

import controlador.Controlador;

public class NuevoUsuario extends JFrame {
	
	private JLabel lTitulo, lNombre, lApellido, lTelefono, lObservacion;
	private JButton bAceptar, bCancelar;
	private JTextField txtNombre, txtApellido, txtTelefono;
	private Controlador controlador;
	


	public NuevoUsuario(Controlador controlador) {
		this.setControlador(controlador);
		setLayout(null);
		setBounds(0,0,350,400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Nuevo Usuario");
		setLocationRelativeTo(null);
		setVisible(false);

		
		lTitulo = new JLabel("Nuevo Usuario");
		lTitulo.setBounds(100,5,200,30);
		lTitulo.setFont(new Font("Verdana", 1, 16));
		lTitulo.setForeground(new Color(0,0,0));
		add(lTitulo);
		
		lNombre = new JLabel("*Nombre");
		lNombre.setBounds(10,50,200,30);
		lNombre.setFont(new Font("Verdana", 1, 14));
		lNombre.setForeground(new Color(0,0,0));
		add(lNombre);
		
		lApellido = new JLabel("*Apellido");
		lApellido.setBounds(10,100,200,30);
		lApellido.setFont(new Font("Verdana", 1, 14));
		lApellido.setForeground(new Color(0,0,0));
		add(lApellido);
		
		lTelefono = new JLabel("*Telefono");
		lTelefono.setBounds(10,150,200,30);
		lTelefono.setFont(new Font("Verdana", 1, 14));
		lTelefono.setForeground(new Color(0,0,0));
		add(lTelefono);
		
		lObservacion = new JLabel("* Campos Obligatorios");
		lObservacion.setBounds(10,250,200,30);
		lObservacion.setFont(new Font("Verdana", 1, 12));
		lObservacion.setForeground(new Color(0,0,0));
		add(lObservacion);
		
		
		txtNombre = new JTextField();
		txtNombre.setBounds(10,80,220,20);
		txtNombre.setBackground(new java.awt.Color(224,224,224));
		txtNombre.setFont(new java.awt.Font("Verdana", 0,14));
		txtNombre.setForeground(new java.awt.Color(0,0,0));
		txtNombre.setEditable(true);
		add(txtNombre);
		
		txtApellido = new JTextField();
		txtApellido.setBounds(10,130,220,20);
		txtApellido.setBackground(new java.awt.Color(224,224,224));
		txtApellido.setFont(new java.awt.Font("Verdana", 0,14));
		txtApellido.setForeground(new java.awt.Color(0,0,0));
		txtApellido.setEditable(true);
		add(txtApellido);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(10,180,220,20);
		txtTelefono.setBackground(new java.awt.Color(224,224,224));
		txtTelefono.setFont(new java.awt.Font("Verdana", 0,14));
		txtTelefono.setForeground(new java.awt.Color(0,0,0));
		txtTelefono.setEditable(true);
		add(txtTelefono);
		
		
		bAceptar = new JButton("Aceptar");
		bAceptar.setBounds(100,290,100,30);
		bAceptar.addActionListener(this.getControlador());
		bAceptar.setEnabled(true);
		add(bAceptar);	
		
		bCancelar = new JButton("Cancelar");
		bCancelar.setBounds(200,290,100,30);
		bCancelar.addActionListener(this.getControlador());
		bCancelar.setEnabled(true);
		add(bCancelar);	
	}
	
	
	
	public JLabel getlTitulo() {
		return lTitulo;
	}



	public void setlTitulo(JLabel lTitulo) {
		this.lTitulo = lTitulo;
	}



	public JButton getbAceptar() {
		return bAceptar;
	}



	public void setbAceptar(JButton bAceptar) {
		this.bAceptar = bAceptar;
	}



	public JButton getbCancelar() {
		return bCancelar;
	}



	public void setbCancelar(JButton bCancelar) {
		this.bCancelar = bCancelar;
	}



	public JTextField getTxtNombre() {
		return txtNombre;
	}



	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}



	public JTextField getTxtApellido() {
		return txtApellido;
	}



	public void setTxtApellido(JTextField txtApellido) {
		this.txtApellido = txtApellido;
	}



	public JTextField getTxtTelefono() {
		return txtTelefono;
	}



	public void setTxtTelefono(JTextField txtTelefono) {
		this.txtTelefono = txtTelefono;
	}



	public Controlador getControlador() {
		return controlador;
	}



	public void setControlador(Controlador controlador) {
		this.controlador = controlador;
	}


}
