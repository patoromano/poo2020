package clases;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {
		String resultado = "";
		Integer bandera = 1;
		String seleccion = "";
		GestorTurnos operaciones = new GestorTurnos();
		do {
			seleccion = (JOptionPane.showInputDialog(null,
					"Selecciona la operación deseada: \n" + "1. Listar Pacientes \n" + "2. Asignar Turnos \n"
							+ "3. Altura Promedio (por Sexo) \n" + "4. Peso Promedio (por Edad) \n"
							+ "5. Ver Turnos \n",
					"Opciones", JOptionPane.PLAIN_MESSAGE, null, new Object[] { "Selecciona", "1", "2", "3", "4", "5" },
					"Selecciona")).toString();

			if (seleccion.equals("1")) {
				String nombre = JOptionPane.showInputDialog("Nombre: ");
				String apellido = JOptionPane.showInputDialog("Apellido: ");
				String sexo = JOptionPane.showInputDialog("Sexo: ");
				Integer edad = Integer.parseInt(JOptionPane.showInputDialog("Edad: "));
				Paciente paciente = new Paciente(nombre, apellido, sexo, edad);
				operaciones.agregarPaciente(paciente);

			}
			if (seleccion.equals("2")) {
				ArrayList<Paciente> arrayP = operaciones.listarPacientes();
				for (Paciente paciente : arrayP) {
					String hora = (JOptionPane.showInputDialog(
							null, "Selecciona la hora a la que desea ingresar al paciente " + paciente.getNombre(),
							"Horas", JOptionPane.PLAIN_MESSAGE, null, new Object[] { "Selecciona", "07:00", "08:00",
									"09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00" },
							"Selecciona")).toString();
					operaciones.asignarTurno(paciente, hora);
				}
			}

			if (seleccion.equals("3")) {
				resultado = operaciones.informeAlturaPromedio();
				JOptionPane.showMessageDialog(null, resultado);
			}

			if (seleccion.equals("4")) {
				resultado = operaciones.informePesoPromedio();
				JOptionPane.showMessageDialog(null, resultado);
			}

			if (seleccion.equals("5")) {
				resultado = operaciones.verTurnos();
				JOptionPane.showMessageDialog(null, resultado);
			}
		} while (bandera == 1);

	}
}
