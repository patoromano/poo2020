package clases;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;

public class GestorTurnos {

	/*
	 * METODO QUE HABIA INTENTADO HACER public ArrayList<Paciente>
	 * agregarPaciente(Paciente paciente) { ArrayList<Paciente> pacientes = new
	 * ArrayList<Paciente>(); //while (userInput.hasNext()) {
	 * pacientes.add(paciente); return pacientes; }
	 */

	/*
	 * public Boolean listarPaciente() { //un metodo para llenar el array con los
	 * pacientes del archivo ArrayList<Paciente> pacientes = agregarPaciente();
	 * 
	 * try (Formatter archivo = new Formatter("assets/pacientes.txt")) {
	 * for(Paciente p : pacientes) { archivo.format("%s %s %s %d %.2f %.2f%n",
	 * p.getNombre(), p.getApellido(), p.getSexo(), p.getEdad(), p.getPeso(),
	 * p.getAltura()); } return true;
	 * 
	 * } catch (FileNotFoundException e) { e.printStackTrace(); return false; }
	 * 
	 * }
	 */
	public ArrayList<Paciente> listarPacientes() {
		// LEER ARCHIVO Y COLOCAR PACIENTES EN UN ARRAY LIST
		ArrayList<Paciente> pacientes = new ArrayList<Paciente>();
		try (Scanner input = new Scanner(Paths.get("assets/pacientes.txt"))) {

			while (input.hasNext()) {
				pacientes.add(new Paciente(input.next(), input.next(), input.next(), input.nextInt(),
						input.nextDouble(), input.nextDouble()));
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return pacientes;
	}

	public Boolean agregarPaciente(Paciente paciente) {
		// AGREGAR UN PACIENTE Y ESCRIBIRLO EN EL ARCHIVO
		ArrayList<Paciente> pacientes = listarPacientes();
		pacientes.add(paciente);
		try (Formatter archivo = new Formatter("assets/pacientes.txt")) {
			for (Paciente p : pacientes) {
				archivo.format("%s %s %s %d %.2f %.2f%n", p.getNombre(), p.getApellido(), p.getSexo(), p.getEdad(),
						p.getPeso(), p.getAltura());
			}
			return true;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}

	}

	public ArrayList<Turno> listarTurnos() {
		// LEER ARCHIVO TURNOS Y COLOCAR TURNOS EN ARRAY LIST
		ArrayList<Turno> turnos = new ArrayList<Turno>();
		try (Scanner input = new Scanner(Paths.get("assets/turnos.txt"))) {
			while (input.hasNext()) {
				turnos.add(new Turno(input.next(), input.next(), input.next()));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return turnos;
	}

	public Boolean asignarTurno(Paciente paciente, String hora) {
		// ASIGNAR UN TURNO A UN PACIENTE Y ESCRIBIRLO EN EL ARCHIVO
		ArrayList<Turno> turnos = listarTurnos();
		turnos.add(new Turno(hora, paciente.getNombre(), paciente.getApellido()));
		try (Formatter output = new Formatter("assets/turnos.txt")) {
			for (Turno t : turnos) {
				output.format("%s %s %s%n", t.getHora(), t.getNombre(), t.getApellido());
			}
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}
	}

	public String informeAlturaPromedio() {
		// INFORME DE ALTURA PROMEDIO SEGUN SEXO
		ArrayList<Paciente> pacientes = listarPacientes();
		Double alturaMasc = 0.0, alturaFem = 0.0;
		Integer totalMasc = 0;
		for (Paciente paciente : pacientes) {
			if (paciente.getSexo().equals("M")) {
				alturaMasc = alturaMasc + paciente.getAltura();
				totalMasc++;
			} else {
				alturaFem = alturaFem + paciente.getAltura();
			}
		}
		return "Las alturas promedio son: \n\n Hombres: " + alturaMasc / totalMasc + "\n Mujeres: "
				+ alturaFem / (pacientes.size() - totalMasc);
	}

	public String informePesoPromedio() {
		// INFORME DE PESO PROMEDIO SEGUN RANGOS DE EDAD
		String informe = "Informe de promedio de peso segun edad:\n";
		ArrayList<Paciente> pacientes = listarPacientes();
		Integer cant1 = 0;
		Integer cant2 = 0;
		Integer cant3 = 0;
		Double sum1 = 0.0;
		Double sum2 = 0.0;
		Double sum3 = 0.0;
		for (int i = 0; i < pacientes.size(); i++) {
			if (pacientes.get(i).getEdad() >= 0 && pacientes.get(i).getEdad() <= 20) {
				sum1 = sum1 + pacientes.get(i).getPeso();
				cant1++;
			}

			if (pacientes.get(i).getEdad() > 20 && pacientes.get(i).getEdad() <= 40) {
				sum2 = sum2 + pacientes.get(i).getPeso();
				cant2++;
			}

			if (pacientes.get(i).getEdad() > 40) {
				sum3 = sum3 + pacientes.get(i).getPeso();
				cant3++;
			}
		}
		informe = informe + "Para el rango: (0 - 20). El peso promedio es: " + sum1 / (cant1) + "\n";
		informe = informe + "Para el rango: (20 - 40). El peso promedio es: " + sum2 / (cant2) + "\n";
		informe = informe + "Para el rango: (40 en adelante). El peso promedio es: " + sum3 / (cant3) + "\n";
		return informe;
	}

	public String verTurnos() {
		String informe = "Los turnos son:\n";
		ArrayList<Turno> arrayT = listarTurnos();
		for (Turno t : arrayT) {
			informe = informe + t.getHora() + " " + t.getNombre() + " " + t.getApellido() + "\n";
		}
		return informe;
	}
	
	

}
