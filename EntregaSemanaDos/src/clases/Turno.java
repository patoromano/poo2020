package clases;

public class Turno {
	
	private String nombre, apellido, hora;
	
	public Turno() {
		this("","","");
	}
	
	public Turno(String hora, String nombre, String apellido) {
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setHora(hora);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

}
