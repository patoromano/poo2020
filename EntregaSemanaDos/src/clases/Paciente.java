package clases;

import java.security.SecureRandom;

public class Paciente {

	private String nombre, apellido, sexo;
	private Integer edad;
	private Double peso, altura;
	
	
	public Paciente(String nombre, String apellido, String sexo, Integer edad, Double peso, Double altura) {
		this(nombre, apellido, sexo, edad);
		this.setPeso(peso);
		this.setAltura(altura);
	}

	public Paciente(String nombre, String apellido, String sexo, Integer edad) {
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setSexo(sexo);
		this.setEdad(edad);
		this.setPeso(autoGenPeso());
		this.setAltura(autoGenAltura());
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}


	public Double getAltura() {
		return altura;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}
	
	private Double autoGen(Double min, Double max) {
		SecureRandom rnd = new SecureRandom();
		Double num = min+(max-min)*rnd.nextDouble();
		return num;
	}
	
	private Double autoGenAltura() {
		Double min = 1.50;
		Double max = 2.00;
		return autoGen(min, max);
	}

	private Double autoGenPeso() {
		Double min = 40.00;
		Double max = 90.00;
		return autoGen(min, max);
	}
	
}