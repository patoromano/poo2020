package entrega;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		System.out.println("Ingrese el tipo de veh�culo que reparar�: ");
		System.out.println(" 	1. Auto.");
		System.out.println(" 	2. Camioneta.");
		System.out.println(" 	3. Cami�n.");
		Scanner input = new Scanner(System.in);
		Integer seleccion = input.nextInt();
		Integer bandera = 1;
		Vehiculo vehiculo = null;

		do {
			switch (seleccion) {
			case 1:
				vehiculo = new Auto();
				break;
			case 2:
				vehiculo = new Camioneta();
				break;
			case 3:
				vehiculo = new Camion();
				break;
			default:
				bandera = 0;
				System.out.println("Debes ingresar una opci�n correcta. ");
				break;
			}
			if (bandera != 0) {
				vehiculo.leerArchivo();
				vehiculo.nombrar();
				vehiculo.pedirDatos();
				vehiculo.operaciones();
				vehiculo.mostrarPrecio();
				vehiculo.guardarArchivo();
			}
		} while (bandera == 0);
	}
}
