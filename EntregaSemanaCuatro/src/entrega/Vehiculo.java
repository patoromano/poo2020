package entrega;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;

public abstract class Vehiculo {

	protected String patente, modelo, tipo, nombre, dni, telefono, averia;
	protected Integer hora;
	protected Double precio, elemento;
	protected String[] array = new String[7];
	protected ArrayList<String[]> arraylist = new ArrayList<String[]>();
	Scanner entrada = new Scanner(System.in);

	public void pedirDatos() {

		System.out.println("-----------------------------------");
		System.out.println("Ingrese los datos del " + tipo + ": ");
		System.out.println("-----------------------------------");

		System.out.println("�Cual es la patente del " + tipo + "? ");
		patente = entrada.nextLine();
		array[0] = patente;

		System.out.println("�Cual es el modelo del " + tipo + "? ");
		modelo = entrada.nextLine();
		array[1] = modelo;

		System.out.println("�Cual es el elemento a reparar? ");
		System.out.println(" 	1. Bomba de agua.");
		System.out.println(" 	2. Frenos.");
		System.out.println(" 	3. Embrague.");
		Integer seleccion = 0;
		seleccion = entrada.nextInt();
		Integer bandera = 1;
		do {
			if (seleccion == 1) {
				averia = " la bomba de agua ";
				elemento = 1000.00;
			} else if (seleccion == 2) {
				averia = " los frenos ";
				elemento = 2000.00;
			} else if (seleccion == 3) {
				averia = " el embrague ";
				elemento = 1500.00;
			} else {
				bandera = 0;
				System.out.println("Ingrese un valor correcto.");
			}
		} while (bandera == 0);
		array[2] = averia;

		System.out.println("�Cuantas horas lleva repararlo? ");
		hora = entrada.nextInt();
		array[3] = "" + hora;

		System.out.println("---------------------------------");
		System.out.println("Ingrese los datos de su due�o: ");
		System.out.println("---------------------------------");

		System.out.print("�Cual es el nombre del due�o? ");
		nombre = entrada.next();
		array[4] = nombre;

		System.out.print("�Cual es el dni del due�o? ");
		dni = entrada.next();
		array[5] = dni;

		System.out.print("�Cual es el n�mero de tel�fono del due�o? ");
		telefono = entrada.next();
		array[6] = telefono;

		System.out.println("-----------------------------------------");
		System.out.println("Informaci�n almacenada correctamente.");
		System.out.println("-----------------------------------------");

		arraylist.add(array);

	}

	public abstract void operaciones();

	public abstract void nombrar();

	public void guardarArchivo() {
		try (Formatter output = new Formatter("assets/vehiculos.csv")) {
			for (String[] array : arraylist) {
				try {
					output.format("%s;%s;%s;%s;%s;%s;%s%n", array[0], array[1], array[2], array[3], array[4], array[5],
							array[6]);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void mostrarPrecio() {
		System.out.println("-------------------------------------------------------------------");
		System.out.println("La reparaci�n de" + averia + "del " + modelo + " cuesta: " + precio);
		System.out.println("-------------------------------------------------------------------");
	}

	public void leerArchivo() {
		try (Scanner input = new Scanner(Paths.get("assets/vehiculos.csv"))) {
			System.out.printf("%-10s%-12s%-12s%10s%n", "Patente", "Modelo", "Aver�a", "Horas", "Due�o", "DNI", "Tel�fono");
			input.useDelimiter("\\r\\n|\\n\\r");
			String[] datos;
			while (input.hasNext()) {
				datos = input.next().split(";");
				arraylist.add(datos);
				System.out.println(datos[0] + "        " + datos[1] + "     " + datos[2] + "     " + datos[3] + "     "
						+ datos[4] + "     " + datos[5] + "     " + datos[6]);

			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
