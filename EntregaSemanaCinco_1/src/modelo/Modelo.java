package modelo;

public class Modelo {
	
	
	private Double num1;
	private Double num2;
	private String operador;
	private Double total;


	
	
	public Double operacion() {

		if (getOperador().equals("+")) {
			total = getNum1() + getNum2();
			return total;
		}
		
		if (getOperador().equals("-")) {
			total = getNum1() - getNum2();
			return total;
		}
		
		if (getOperador().equals("*")) {
			total = getNum1() * getNum2();
			return total;
		}
		
		if (getOperador().equals("/")) {
			total = getNum1() / getNum2();
			return total;
		}
		
		
		return 23.0;
	}
	
	
	public Double getNum1() {
		return num1;
	}
	public void setNum1(Double num1) {
		this.num1 = num1;
	}
	public Double getNum2() {
		return num2;
	}
	public void setNum2(Double num2) {
		this.num2 = num2;
	}
	public String getOperador() {
		return operador;
	}
	public void setOperador(String operador) {
		this.operador = operador;
	}
	public Double getTotal() {
		return total;
	}

	

}
