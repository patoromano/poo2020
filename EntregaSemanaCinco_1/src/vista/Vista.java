package vista;

import java.awt.*;
import javax.swing.*;

import controlador.Controlador;

public class Vista extends JFrame {
	
	public JTextField casilla;
	public JButton cero, uno, dos, tres, cuatro, cinco, seis, siete, ocho, nueve;
	public JButton clean, add, sub, div, mult, point, equal, dot;
	
	
	public Vista(Controlador controlador) {
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setBackground(new Color(50, 100, 100));
		setTitle("Calculadora MVC");
		setBounds(0,0,280,390);
		setVisible(true);
		setResizable(false);
		setLocationRelativeTo(null);
		
		
		casilla = new JTextField();
		casilla.setBounds(25,15,215,35);
		casilla.setBackground(new java.awt.Color(224,224,224));
		casilla.setFont(new java.awt.Font("Verdana", 1,14));
		casilla.setForeground(new java.awt.Color(0,0,0));
		add(casilla);
		
		cero = new JButton("0");
		cero.setBounds(25,285,105,50);
		cero.setBackground(new Color(224,224,224));
		cero.setFont(new Font("Verdana", 1,14));
		cero.setForeground(new Color(0,0,0));
		cero.addActionListener(controlador);
		add(cero);
		
		uno = new JButton("1");
		uno.setBounds(25,230,50,50);
		uno.setBackground(new Color(224,224,224));
		uno.setFont(new Font("Verdana", 1,14));
		uno.setForeground(new Color(0,0,0));
		uno.addActionListener(controlador);
		add(uno);
		
		dos = new JButton("2");
		dos.setBounds(80,230,50,50);
		dos.setBackground(new Color(224,224,224));
		dos.setFont(new Font("Verdana", 1,14));
		dos.setForeground(new Color(0,0,0));
		dos.addActionListener(controlador);
		add(dos);
		
		tres = new JButton("3");
		tres.setBounds(135,230,50,50);
		tres.setBackground(new Color(224,224,224));
		tres.setFont(new Font("Verdana", 1,14));
		tres.setForeground(new Color(0,0,0));
		tres.addActionListener(controlador);
		add(tres);
		
		cuatro = new JButton("4");
		cuatro.setBounds(25,175,50,50);
		cuatro.setBackground(new Color(224,224,224));
		cuatro.setFont(new Font("Verdana", 1,14));
		cuatro.setForeground(new Color(0,0,0));
		cuatro.addActionListener(controlador);
		add(cuatro);
		
		cinco = new JButton("5");
		cinco.setBounds(80,175,50,50);
		cinco.setBackground(new Color(224,224,224));
		cinco.setFont(new Font("Verdana", 1,14));
		cinco.setForeground(new Color(0,0,0));
		cinco.addActionListener(controlador);
		add(cinco);
		
		seis = new JButton("6");
		seis.setBounds(135,175,50,50);
		seis.setBackground(new Color(224,224,224));
		seis.setFont(new Font("Verdana", 1,14));
		seis.setForeground(new Color(0,0,0));
		seis.addActionListener(controlador);
		add(seis);
		
		siete = new JButton("7");
		siete.setBounds(25,120,50,50);
		siete.setBackground(new Color(224,224,224));
		siete.setFont(new Font("Verdana", 1,14));
		siete.setForeground(new Color(0,0,0));
		siete.addActionListener(controlador);
		add(siete);
		
		ocho = new JButton("8");
		ocho.setBounds(80,120,50,50);
		ocho.setBackground(new Color(224,224,224));
		ocho.setFont(new Font("Verdana", 1,14));
		ocho.setForeground(new Color(0,0,0));
		ocho.addActionListener(controlador);
		add(ocho);
		
		nueve = new JButton("9");
		nueve.setBounds(135,120,50,50);
		nueve.setBackground(new Color(224,224,224));
		nueve.setFont(new Font("Verdana", 1,14));
		nueve.setForeground(new Color(0,0,0));
		nueve.addActionListener(controlador);
		add(nueve);
		
		clean = new JButton("C");
		clean.setBounds(25,65,50,50);
		clean.setBackground(new Color(224,224,224));
		clean.setFont(new Font("Verdana", 1,14));
		clean.setForeground(new Color(0,0,0));
		clean.addActionListener(controlador);
		add(clean);
		
		equal = new JButton("=");
		equal.setBounds(190,230,50,105);
		equal.setBackground(new Color(224,224,224));
		equal.setFont(new Font("Verdana", 1,14));
		equal.setForeground(new Color(0,0,0));
		equal.addActionListener(controlador);
		add(equal);
		
		add = new JButton("+");
		add.setBounds(190,120,50,105);
		add.setBackground(new Color(224,224,224));
		add.setFont(new Font("Verdana", 1,14));
		add.setForeground(new Color(0,0,0));
		add.addActionListener(controlador);
		add(add);
		
		sub = new JButton("-");
		sub.setBounds(190,65,50,50);
		sub.setBackground(new Color(224,224,224));
		sub.setFont(new Font("Verdana", 1,14));
		sub.setForeground(new Color(0,0,0));
		sub.addActionListener(controlador);
		add(sub);
		
		div = new JButton("/");
		div.setBounds(80,65,50,50);
		div.setBackground(new Color(224,224,224));
		div.setFont(new Font("Verdana", 1,14));
		div.setForeground(new Color(0,0,0));
		div.addActionListener(controlador);
		add(div);
		
		mult = new JButton("*");
		mult.setBounds(135,65,50,50);
		mult.setBackground(new Color(224,224,224));
		mult.setFont(new Font("Verdana", 1,14));
		mult.setForeground(new Color(0,0,0));
		mult.addActionListener(controlador);
		add(mult);
		
		dot = new JButton(".");
		dot.setBounds(135,285,50,50);
		dot.setBackground(new Color(224,224,224));
		dot.setFont(new Font("Verdana", 1,14));
		dot.setForeground(new Color(0,0,0));
		dot.addActionListener(controlador);
		add(dot);
		

		
		
		}
	
	
	public void setCasilla(String valor) {
		casilla.setText(casilla.getText() + valor);
	}
	
	public Boolean checkTexto(String texto) {
		return texto.matches("[0-9.]*");
	}
	
	public Double getCasilla() {
		
		if ( checkTexto(casilla.getText().trim()) && !casilla.getText().trim().equals(""))  {
			return Double.parseDouble(casilla.getText());
		} else {
			JOptionPane.showMessageDialog(null, "Debes ingresar numeros.");
			return 0.0;
		}
	}
	
	public void limpiar() {
		casilla.setText("");
	}


}
