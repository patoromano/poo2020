package controlador;

import java.awt.event.*;

import javax.swing.AbstractButton;
import javax.swing.JButton;

import org.omg.CosNaming._BindingIteratorImplBase;

import modelo.Modelo;
import vista.Vista;

public class Controlador implements ActionListener {

	Vista vista;
	Modelo modelo;

	public Controlador() {
		this.vista = new Vista(this);
		this.modelo = new Modelo();
	}

	public void actionPerformed(ActionEvent e) {

		JButton btn = (JButton) e.getSource();

		switch (btn.getText()) {
		case "C":
			vista.limpiar();
			break;
		case "=":
			modelo.setNum2(vista.getCasilla());
			vista.casilla.setText(Double.toString(modelo.operacion()));
			break;
		default:
			if(vista.checkTexto(btn.getText())) {
				vista.setCasilla(btn.getText());
			} else {
				modelo.setOperador(btn.getText());
				modelo.setNum1(vista.getCasilla());
				vista.limpiar();
			}
		}
	}

}
