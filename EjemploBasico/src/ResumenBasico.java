
public class ResumenBasico {	//RESUMEN ROMANO PABLO

	public static void main(String[] args) {
		
		//comentario de linea simple
		
		/* comentario 
		 * de multiples lineas
		 */
		
		/** comentario JAVADOC
		 * 
		 */
		
		//TIPOS PRIMITIVOS
		int numEntero = 8;
		double numFlotante = 2.3;
		boolean booleano = true;
		char caracter = 'A';
		
		int casteo = (int) 3.4;
		
		System.out.println("numero " + casteo);
		
		
		//OPERADORES LOGICOS
		if (numEntero == numFlotante) {
			System.out.println("Los numeros son iguales ");
	}	else if (numEntero > numFlotante) {
			System.out.println("El numero entero es mayor al flotante ");
	}	else {
			System.out.println("El numero entero es menor o igual al flotante ");
	}
		
		/* igualdad: ==
		 * desigualdad: !=
		 * mayor: >
		 * menor: <
		 * mayor o igual: >=
		 * menor o igual <=
		 */
			
		if(numEntero != numFlotante && numEntero > 7) {
			System.out.println("El numero entero es distinto al flotante y mayor a siete ");		
		}
		
		/*	& and
		 * 	| or
		 * 	&& and, no evaluando la segunda parte de la condicion si la primera es falsa
		 * 	|| or, no evaluando la segunda parte de la condicion si la primera es verdadera
		 * 	^ XOR
		 */

		System.out.println(numEntero > numFlotante? "E>F":"E<=F");
		
		
		int resultado = numEntero !=0? 100/numEntero : 0;
		
		switch (caracter) {
		case 'A' :
			System.out.println("El valor del caracter es 'A' ");
			break;
		case 'B' :
			System.out.println("El valor del caracter es 'B' ");
			break;
		default:
			throw new IllegalArgumentException("Unexpected value " +caracter);		
		}
		
		
		

		int[] numEnteros = {1, 3, 5, 7, 9, 11};
		
		
		
		//BUCLES
		
		int i = 0;
		
		while (i < numEnteros.length) {
			System.out.println(numEnteros[i]);
			i++;
		}
		
		int j = 5;
		
		do {
			numEnteros[j] = j;
			System.out.println("num en do while: " + numEnteros[j]);
			j--;
		}
		while(j<=0);
		
		
		
		for (int k = 0; k < numEnteros.length; k++) {
			System.out.println("en for: "+ numEnteros[k]);
		}
		
		
		//OPERADOES DE ASIGNACION COMPUESTOS
		
		i+=7; //igual a i = i+7
		i-=4;
		i*=5;
		i/=3;
		i%=9;
	
		
		i++; // usa el valor y luego incrementa
		++i; // incrementa y luego usa el valor

	}

}

//esta bien el resumen 